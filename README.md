# Python `import`: modules and packages

Today (2022-03-28) we'll talk about python `import` statement, as well as **modules** and **packages**.

It's mostly livecoding/codealong, so the `.py` files in the repo won't make too much sense outside of context (as they barely have any code).

We'll check out python's official documentation on the subject [here](https://docs.python.org/3/tutorial/modules.html).

If you'd like to dig deeper into the details of import, modules and packages, [this PyCon talk](https://www.youtube.com/watch?v=0oTh1CXRaQ0) is recommended.