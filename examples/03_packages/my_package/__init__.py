# Uncomment the following line to trigger prints when importing
print("My package is being initialized!")
print(__name__)

# Uncomment to showcase how to avoid nested imports
#
from .mod1 import x as x1
from .mod2 import x as x2

# Uncomment to showcase __all__
#
sum_ = x1 + x2
prod = x1 * x2

__all__ = ["sum_", "prod"]
