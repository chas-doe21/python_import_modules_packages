# Uncomment this and run module_1 to trigger a circular import
import module_1

print(module_number, module_1.module_number)

# Move this line to the top of the file to un-trigger the circular import
module_number = 2
