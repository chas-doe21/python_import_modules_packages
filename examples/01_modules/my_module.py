# This is a module. It prints something whenever it is imported or run

if __name__ == "__main__":
    print("You ran my_module.py")
else:
    print("You imported my_module.py")
